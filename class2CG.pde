float yAngle;

void setup() {
  
  size(800, 800, P3D);
  
  // color of the stroke
  stroke(255);
  
  // don't fill up geometry
  noFill();
  yAngle = 0;  
}

// similar to update
void draw() {
  
  background(0);
  
  // first - super quick
  if(key == 'q') println("YOU PRESSED THE Q!!");
  
  yAngle += 0.1f;
  
  // transformations are being applied to a matrix that contains the current state of the world
  
  // HOW TO ISOLATE TRANSFORMATIONS 
  
  // save the current state first
  pushMatrix();
  
  // transformations in processing (very similar to opengl)
  translate(width/2, height/2, 0);
  rotateZ(30);
  rotateY(yAngle);

  // whenever we draw something all the transformations applied to the matrix will affect this object
  // basic shapes
  box(100);
  
  pushMatrix();
  translate(200, 0, 0);
  box(50);
  
  popMatrix();
  
  translate(0, 200, 0);
  sphere(50);
  
  // reestablish the previous state in the matrix
  popMatrix();
  
  
  // if we do new transformations they will not apply to previously drawn objects
  translate(width/2 + 200, height/2 - 200, 0);
  rotateX(20);
  scale(0.1);
  sphere(300);
  
  // how the hell do we isolate transformations?!
  // use the sack
  box(100);
  
  //sphere(100);
}
